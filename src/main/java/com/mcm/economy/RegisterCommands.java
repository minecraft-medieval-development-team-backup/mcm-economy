package com.mcm.economy;

import com.mcm.economy.commands.CommandShop;
import com.mcm.economy.commands.CommandsMercado;
import com.mcm.economy.commands.CommandsShop;

public class RegisterCommands {

    public static void register() {
        Main.instance.getCommand("shop").setExecutor(new CommandShop());
        Main.instance.getCommand("loja").setExecutor(new CommandShop());
        Main.instance.getCommand("shopconfig").setExecutor(new CommandsShop());
        Main.instance.getCommand("lojaconfig").setExecutor(new CommandsShop());
        Main.instance.getCommand("mercado").setExecutor(new CommandsMercado());
        Main.instance.getCommand("market").setExecutor(new CommandsMercado());
    }
}
