package com.mcm.economy;

import com.mcm.economy.inventoryListeners.InventoryCategory;
import com.mcm.economy.inventoryListeners.InventoryItens;
import com.mcm.economy.inventoryListeners.InventoryMercado;
import com.mcm.economy.listeners.CreateShopEvent;
import com.mcm.economy.listeners.PlayerBreakShopEvent;
import com.mcm.economy.listeners.PlayerInteractShopEvent;
import com.mcm.economy.listeners.SetItemOnShopEvent;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new InventoryCategory(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryItens(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new CreateShopEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new SetItemOnShopEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerInteractShopEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerBreakShopEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryMercado(), Main.plugin);
    }
}
