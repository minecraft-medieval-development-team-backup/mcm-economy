package com.mcm.economy.cache;

import java.util.HashMap;

public class PlayerPage {

    private String uuid;
    private String category;
    private int page;
    private static HashMap<String, PlayerPage> cache = new HashMap<String, PlayerPage>();

    public PlayerPage(String uuid, String category, int page) {
        this.uuid = uuid;
        this.category = category;
        this.page = page;
    }

    public PlayerPage insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static PlayerPage get(String uuid) {
        return cache.get(uuid);
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
