package com.mcm.economy.commands;

import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.cache.PlayerQtMarket;
import com.mcm.core.database.MercadoDb;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.database.TagDb;
import com.mcm.core.enums.ServersList;
import com.mcm.economy.inventoryListeners.InventoryMercado;
import com.mcm.economy.managers.MercadoBuilder;
import com.mcm.economy.utils.CategorySeparator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandsMercado implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = TagDb.getTag(uuid);

        if (command.getName().equalsIgnoreCase("mercado") || command.getName().equalsIgnoreCase("market")) {
            if (args.length == 0) {
                player.openInventory(MercadoBuilder.main(uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("remover") || args[0].equalsIgnoreCase("remove")) {
                    MercadoBuilder.openChestPrivate(player, uuid);
                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("anunciar") || args[0].equalsIgnoreCase("announce")) {
                    ItemStack item = player.getItemInHand().clone();

                    if (item != null && !item.getType().equals(Material.AIR)) {
                        int quantity;
                        if (PlayerQtMarket.get(uuid) != null) quantity = PlayerQtMarket.get(uuid).getQuantity(); else quantity = 0;

                        if (!tag.equals("membro")) {
                            if (quantity < 9) {
                                if (CategorySeparator.getCategory(item) != null) {
                                    try {
                                        double value = Double.parseDouble(args[1]);
                                        MercadoDb.addItem(uuid, player.getName(), CategorySeparator.getCategory(item), item, value);

                                        player.sendMessage(Main.getTradution("S5PB7Yw&BS#Bkjp", uuid));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);

                                        for (ServersList server : ServersList.values()) {
                                            if (!server.name().replaceAll("_", "-").equals(Main.server_name)) RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/publishing=%a3ARh5aADJdAJp=" + player.getName()
                                            + "=9!!SAYkY?#y2qSC=" + item.getType().name() + "=A#CBx3Fg$4CSJkZ=" + MercadoBuilder.formatter.format(value) + "=A!R3FbUnkGd?z$6");
                                        }

                                        for (Player target : Bukkit.getOnlinePlayers()) {
                                            target.sendMessage(Main.getTradution("%a3ARh5aADJdAJp", target.getUniqueId().toString()) + player.getName() + Main.getTradution("9!!SAYkY?#y2qSC", target.getUniqueId().toString()) + item.getType().name() + Main.getTradution("A#CBx3Fg$4CSJkZ", target.getUniqueId().toString()) + MercadoBuilder.formatter.format(value) + Main.getTradution("A!R3FbUnkGd?z$6", target.getUniqueId().toString()));
                                            target.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        }
                                        player.getItemInHand().setAmount(0);
                                        player.updateInventory();
                                    } catch (NumberFormatException e) {
                                        player.sendMessage(Main.getTradution("5Z*8qY6?fNEY3#g", uuid));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(Main.getTradution("*m5BK#CJwuAdT3D", uuid));
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(Main.getTradution("$RXDUU3Fkejtq@%", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            if (quantity < 2) {
                                if (CategorySeparator.getCategory(item) != null) {
                                    try {
                                        double value = Double.parseDouble(args[1]);
                                        MercadoDb.addItem(uuid, player.getName(), CategorySeparator.getCategory(item), item, value);
                                        player.getItemInHand().setType(Material.AIR);
                                        player.updateInventory();

                                        player.sendMessage(Main.getTradution("S5PB7Yw&BS#Bkjp", uuid));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                    } catch (NumberFormatException e) {
                                        player.sendMessage(Main.getTradution("5Z*8qY6?fNEY3#g", uuid));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(Main.getTradution("*m5BK#CJwuAdT3D", uuid));
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            } else {
                                player.sendMessage(Main.getTradution("$RXDUU3Fkejtq@%", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        player.sendMessage(Main.getTradution("6z&U%a#5ggsBQ?T", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            } else {
                //-
            }
        }
        return false;
    }
}
