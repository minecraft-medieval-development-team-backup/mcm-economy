package com.mcm.economy.commands;

import com.mcm.core.Main;
import com.mcm.core.cache.StoreGUI;
import com.mcm.core.database.StoreDb;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class CommandsShop implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};
        if (command.getName().equalsIgnoreCase("shopconfig") || command.getName().equalsIgnoreCase("lojaconfig")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("removeitem")) {
                        boolean removed = StoreDb.rmvItemOnGUIwithItemID(args[1]);
                        if (removed == true) {
                            player.sendMessage(Main.getTradution("TkkS2Ex6R&NVAsM", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("x2C!d2J&7KUzCjk", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("J&z6bkhDHYcTm94", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }

                    if (args[0].equalsIgnoreCase("removecategory") || args[0].equalsIgnoreCase("removercategoria")) {
                        if (com.mcm.core.database.StoreDb.getCategoryItemGUI(args[1]) != null) {
                            com.mcm.core.database.StoreDb.deleteCategoryGUI(args[1]);
                            player.sendMessage(Main.getTradution("2tynXJC?xqq7NV!", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("X*v6HfG9%*TjQb4", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }

                    if (args[0].equalsIgnoreCase("addcategory") || args[0].equalsIgnoreCase("addcategoria")) {
                        if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR)) {
                            ItemStack item = player.getItemInHand().clone();
                            ItemMeta meta = item.getItemMeta();
                            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                            meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                            meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                            meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
                            meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
                            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                            meta.setDisplayName(ChatColor.YELLOW + " * " + args[1].replaceAll("_", " "));
                            item.setItemMeta(meta);

                            com.mcm.core.database.StoreDb.createNewCategoryGUI(args[1].replaceAll("_", " "), item);
                            player.sendMessage(Main.getTradution("7%NCF2W*?57Z%zG", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("&aCS54Z%8mFE4Vm", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("J&z6bkhDHYcTm94", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("setamount") || args[0].equalsIgnoreCase("setquantia")) {
                        updateValue(player, uuid, args, "q");
                    }

                    if (args[0].equalsIgnoreCase("setpurchase") || args[0].equalsIgnoreCase("setcompra")) {
                        updateValue(player, uuid, args, "c");
                    }

                    if (args[0].equalsIgnoreCase("setsale") || args[0].equalsIgnoreCase("setvenda")) {
                        updateValue(player, uuid, args, "v");
                    }

                    if (args[0].equalsIgnoreCase("updatecategoryname") || args[0].equalsIgnoreCase("updatecategorianome")) {
                        if (com.mcm.core.database.StoreDb.getCategoryItemGUI(args[1]) != null) {
                            ItemStack item = com.mcm.core.database.StoreDb.getCategoryItemGUI(args[1]);
                            ItemMeta meta = item.getItemMeta();
                            meta.setDisplayName(ChatColor.YELLOW + " * " + args[2]);
                            item.setItemMeta(meta);
                            com.mcm.core.database.StoreDb.updateCategoryItemGUI(args[1], item);
                            player.sendMessage(Main.getTradution("!w7Qx@9yDZdTrZH", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("X*v6HfG9%*TjQb4", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("J&z6bkhDHYcTm94", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args.length >= 3 && args.length <= 4) {
                    if (args[0].equalsIgnoreCase("additem")) {
                        if (com.mcm.core.database.StoreDb.getCategoryItemGUI(args[1]) != null) {
                            if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR)) {
                                if (args.length == 3) {
                                    try {
                                        if (args[2].contains("v")) {
                                            int value = Integer.valueOf(args[2].replaceAll("v", ""));
                                            com.mcm.core.database.StoreDb.addItemOnCategoryGUI(args[1], player.getItemInHand().getType().name() + "-" + player.getItemInHand().getDurability() + ":" + player.getItemInHand().getAmount() + ":v" + value + ":c0:vV0:cV0");
                                        } else if (args[2].contains("c")) {
                                            int value = Integer.valueOf(args[2].replaceAll("c", ""));
                                            com.mcm.core.database.StoreDb.addItemOnCategoryGUI(args[1], player.getItemInHand().getType().name() + "-" + player.getItemInHand().getDurability() + ":" + player.getItemInHand().getAmount() + ":v0:c" + value + ":vV0:cV0");
                                        } else {
                                            player.sendMessage(Main.getTradution("DMTH!d*ZQu97cNX", uuid));
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                            return false;
                                        }

                                        player.sendMessage(Main.getTradution("%7nfv*M?j?S*v8&", uuid));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    } catch (NumberFormatException e) {
                                        player.sendMessage(Main.getTradution("5Z*8qY6?fNEY3#g", uuid));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    try {
                                        if (args[2].contains("c") && args[3].contains("v")) {
                                            int valueC = Integer.valueOf(args[2].replaceAll("c", ""));
                                            int valueV = Integer.valueOf(args[3].replaceAll("v", ""));
                                            Bukkit.broadcastMessage(valueC + ":" + valueV);
                                            com.mcm.core.database.StoreDb.addItemOnCategoryGUI(args[1], player.getItemInHand().getType().name() + "-" + player.getItemInHand().getDurability() + ":" + player.getItemInHand().getAmount() + ":v" + valueV + ":c" + valueC + ":vV0:cV0");

                                            player.sendMessage(Main.getTradution("%7nfv*M?j?S*v8&", uuid));
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        } else {
                                            player.sendMessage(Main.getTradution("DMTH!d*ZQu97cNX", uuid));
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                        }
                                    } catch (NumberFormatException e) {
                                        player.sendMessage(Main.getTradution("5Z*8qY6?fNEY3#g", uuid));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                }
                            } else {
                                player.sendMessage(Main.getTradution("YyGamN!Ug5hj3xX", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(Main.getTradution("5azc?4Z*aUf%DV6", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("J&z6bkhDHYcTm94", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("J&z6bkhDHYcTm94", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }

    private static void updateValue(Player player, String uuid, String[] args, String type) {
        try {
            int value = Integer.valueOf(args[2]);
            boolean updated = false;
            for (String categories : StoreGUI.categories) {
                for (String id : StoreGUI.get(categories).getIds()) {
                    if (id.equalsIgnoreCase(args[1])) {
                        for (String item : StoreGUI.get(categories).getItens()) {
                            if (item.contains(args[1])) {
                                String[] split = item.split(":");
                                String update = null;
                                int a = 0;
                                for (String s : split) {
                                    if (type.equals("q")) {
                                        if (a == 1) {
                                            update += ":" + value;
                                        } else {
                                            if (update == null) {
                                                update = s;
                                            } else update += ":" + s;
                                        }
                                    } else if (!s.contains(type)) {
                                        if (update == null) {
                                            update = s;
                                        } else update += ":" + s;
                                    } else {
                                        update += ":" + type + value;
                                    }
                                    a++;
                                }
                                StoreDb.updateItemGUI(categories, item, update);
                                if (type.equals("c")) {
                                    player.sendMessage(Main.getTradution("UA%pme85jVQg3D?", uuid));
                                } else if (type.equals("v")) {
                                    player.sendMessage(Main.getTradution("%3tzfmKWeAr72T6", uuid));
                                } else if (type.equals("q")) {
                                    player.sendMessage(Main.getTradution("", uuid));
                                }
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        }
                    }
                }
            }

            if (updated == false) {
                player.sendMessage(Main.getTradution("x2C!d2J&7KUzCjk", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        } catch (NumberFormatException e) {
            player.sendMessage(Main.getTradution("H%6D*97mdD9$mdH", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }
}
