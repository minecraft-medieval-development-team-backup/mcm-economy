package com.mcm.economy.inventoryListeners;

import com.mcm.economy.Main;
import com.mcm.economy.managers.ShopBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryCategory implements Listener {

    @EventHandler
    public void onInteractInv(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase(com.mcm.core.Main.getTradution("K7q9?n5tM$7s@8U", uuid))) {
            event.setCancelled(true);

            if (event.getClickedInventory().getItem(event.getSlot()) != null && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        String category = event.getClickedInventory().getItem(event.getSlot()).getItemMeta().getDisplayName();
                        player.openInventory(ShopBuilder.itens(uuid, category.substring(5), 1));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }, 6L);
            }
        }
    }
}
