package com.mcm.economy.inventoryListeners;

import com.mcm.core.Main;
import com.mcm.core.database.CoinsDb;
import com.mcm.core.database.StoreDb;
import com.mcm.core.utils.InventoryHasSpace;
import com.mcm.economy.cache.PlayerPage;
import com.mcm.economy.managers.ShopBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;

public class InventoryItens implements Listener {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    @EventHandler
    public void onInteractInv(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase(com.mcm.core.Main.getTradution("8G6YG#g6VM!VYgm", uuid))) {
            event.setCancelled(true);

            //RIGHT_CLICK
            //  /Material-id:quantia:vVALUE:cVALUE:vVEZES:cVEZES:#id/
            if (event.getClickedInventory().getItem(event.getSlot()) != null && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                if (event.getClick().isRightClick() && event.getClick().isShiftClick()) {
                    String[] split = event.getClickedInventory().getItem(event.getSlot()).getItemMeta().getLocalizedName().split(":");
                    double value = Double.parseDouble(split[3].replaceAll("c", ""));
                    double oldcoins = com.mcm.core.database.CoinsDb.getCoins(uuid);

                    double difference = 0;
                    for (String category : StoreDb.categories()) {
                        for (String itens : StoreDb.getItensGUI(category)) {
                            if (itens.contains(event.getClickedInventory().getItem(event.getSlot()).getType().name())) {
                                String[] spl = itens.split(":");
                                difference = value - ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[5]), true);
                                if (difference == value) difference = 0;
                            }
                        }
                    }

                    int space = InventoryHasSpace.hasSpace(player.getInventory(), event.getClickedInventory().getItem(event.getSlot()));
                    double perItem = (value - difference) / event.getClickedInventory().getItem(event.getSlot()).getAmount();
                    if (oldcoins >= perItem * space) {
                        if (space >= event.getClickedInventory().getItem(event.getSlot()).getAmount()) {
                            com.mcm.core.database.CoinsDb.updateCoins(uuid, (int) (oldcoins - (perItem * space)));

                            player.getInventory().addItem(new ItemStack(event.getClickedInventory().getItem(event.getSlot()).getType(), space, event.getClickedInventory().getItem(event.getSlot()).getDurability()));
                            player.sendMessage(Main.getTradution("?*HGUZ22%Mph9%k", uuid) + formatter.format(perItem * space));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("R9zFuPN27*4!Bp9", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("b3qUyt2@%F4N!hq", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (event.getClick().isRightClick() && !event.getClick().isShiftClick()) {
                    String[] split = event.getClickedInventory().getItem(event.getSlot()).getItemMeta().getLocalizedName().split(":");
                    double value = Double.parseDouble(split[3].replaceAll("c", ""));
                    double oldcoins = com.mcm.core.database.CoinsDb.getCoins(uuid);

                    double difference = 0;
                    for (String category : StoreDb.categories()) {
                        for (String itens : StoreDb.getItensGUI(category)) {
                            if (itens.contains(event.getClickedInventory().getItem(event.getSlot()).getType().name())) {
                                String[] spl = itens.split(":");
                                difference = value - ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[5]), true);
                                if (difference == value) difference = 0;
                            }
                        }
                    }

                    if (oldcoins >= (value - difference)) {
                        int space = InventoryHasSpace.hasSpace(player.getInventory(), event.getClickedInventory().getItem(event.getSlot()));
                        if (space >= event.getClickedInventory().getItem(event.getSlot()).getAmount()) {
                            com.mcm.core.database.CoinsDb.updateCoins(uuid, (int) (oldcoins - (value - difference)));

                            player.getInventory().addItem(new ItemStack(event.getClickedInventory().getItem(event.getSlot()).getType(), event.getClickedInventory().getItem(event.getSlot()).getAmount(), event.getClickedInventory().getItem(event.getSlot()).getDurability()));
                            player.sendMessage(Main.getTradution("?*HGUZ22%Mph9%k", uuid) + formatter.format(value - difference));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("R9zFuPN27*4!Bp9", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("b3qUyt2@%F4N!hq", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }

                //LEFT_CLICK
                if (event.getClick().isLeftClick() && event.getClick().isShiftClick()) {
                    int quantity = 0;
                    for (int i = 0; i <= player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType().equals(event.getClickedInventory().getItem(event.getSlot()).getType()) && event.getClickedInventory().getItem(event.getSlot()).getDurability() == player.getInventory().getItem(i).getDurability()) {
                            quantity += player.getInventory().getItem(i).getAmount();
                        }
                    }
                    String[] split = event.getClickedInventory().getItem(event.getSlot()).getItemMeta().getLocalizedName().split(":");
                    double value = Double.parseDouble(split[2].replaceAll("v", ""));
                    double oldcoins = com.mcm.core.database.CoinsDb.getCoins(uuid);

                    double difference = 0;
                    for (String category : StoreDb.categories()) {
                        for (String itens : StoreDb.getItensGUI(category)) {
                            if (itens.contains(event.getClickedInventory().getItem(event.getSlot()).getType().name())) {
                                String[] spl = itens.split(":");
                                difference = value - ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[5]), true);
                                if (difference == value) difference = 0;
                            }
                        }
                    }

                    if (quantity >= event.getClickedInventory().getItem(event.getSlot()).getAmount()) {
                        com.mcm.core.database.CoinsDb.updateCoins(uuid, (int) (oldcoins + ((value + difference) * (quantity / 64))));

                        ItemStack item = new ItemStack(event.getClickedInventory().getItem(event.getSlot()).getType(), event.getClickedInventory().getItem(event.getSlot()).getAmount() * (quantity / 64), event.getClickedInventory().getItem(event.getSlot()).getDurability());
                        com.mcm.core.utils.RemoveItemOfInventory.removeItemsFromType(player.getInventory(), item, item.getAmount());
                        player.sendMessage(Main.getTradution("maPQV8DU3u?E6vx", uuid) + formatter.format((value + difference) * (quantity / 64)));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(Main.getTradution("#t5gvpqtwj%ZMGC", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (event.getClick().isLeftClick() && !event.getClick().isShiftClick()) {
                    int quantity = 0;
                    for (int i = 0; i <= player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType().equals(event.getClickedInventory().getItem(event.getSlot()).getType()) && event.getClickedInventory().getItem(event.getSlot()).getDurability() == player.getInventory().getItem(i).getDurability()) {
                            quantity += player.getInventory().getItem(i).getAmount();
                        }
                    }
                    String[] split = event.getClickedInventory().getItem(event.getSlot()).getItemMeta().getLocalizedName().split(":");
                    double value = Double.parseDouble(split[2].replaceAll("v", ""));
                    double oldcoins = com.mcm.core.database.CoinsDb.getCoins(uuid);

                    double difference = 0;
                    for (String category : StoreDb.categories()) {
                        for (String itens : StoreDb.getItensGUI(category)) {
                            if (itens.contains(event.getClickedInventory().getItem(event.getSlot()).getType().name())) {
                                String[] spl = itens.split(":");
                                difference = value - ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[5]), true);
                                if (difference == value) difference = 0;
                            }
                        }
                    }

                    if (quantity >= event.getClickedInventory().getItem(event.getSlot()).getAmount()) {
                        com.mcm.core.database.CoinsDb.updateCoins(uuid, (int) (oldcoins + (value + difference)));

                        ItemStack item = new ItemStack(event.getClickedInventory().getItem(event.getSlot()).getType(), event.getClickedInventory().getItem(event.getSlot()).getAmount(), event.getClickedInventory().getItem(event.getSlot()).getDurability());
                        com.mcm.core.utils.RemoveItemOfInventory.removeItemsFromType(player.getInventory(), item, item.getAmount());
                        player.sendMessage(Main.getTradution("YBsDa3hgu%Ynp3f", uuid) + formatter.format(value + difference));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(Main.getTradution("#t5gvpqtwj%ZMGC", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }

            if (event.getSlot() == 45 && event.getInventory().getItem(event.getSlot()) != null && !event.getInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(ShopBuilder.itens(uuid, PlayerPage.get(uuid).getCategory(), PlayerPage.get(uuid).getPage() - 1));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }, 6L);
            }

            if (event.getSlot() == 53 && event.getInventory().getItem(event.getSlot()) != null && !event.getInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.openInventory(ShopBuilder.itens(uuid, PlayerPage.get(uuid).getCategory(), PlayerPage.get(uuid).getPage() + 1));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                }, 6L);
            }
        }
    }
}
