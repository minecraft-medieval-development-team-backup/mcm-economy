package com.mcm.economy.inventoryListeners;

import com.mcm.core.RabbitMq;
import com.mcm.core.cache.ItensMercado;
import com.mcm.core.database.CoinsDb;
import com.mcm.core.database.MercadoDb;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.enums.ServersList;
import com.mcm.core.utils.InventoryHasSpace;
import com.mcm.economy.Main;
import com.mcm.economy.cache.PlayerPage;
import com.mcm.economy.managers.MercadoBuilder;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class InventoryMercado implements Listener {

    @EventHandler
    public void onInteractInv(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final String uuid = player.getUniqueId().toString();

        //shop
        String[] invsName = {com.mcm.core.Main.getTradution("AmgCX!5M9gV@*!J", uuid).substring(2) + ": ", com.mcm.core.Main.getTradution("W#3mA8F$tjXkN*j", uuid).substring(2) + ": ", com.mcm.core.Main.getTradution("v4G?ekFct23Dvvc", uuid).substring(2) + ": "
                , com.mcm.core.Main.getTradution("P8DRv#g*p?E4*nE", uuid).substring(2) + ": ", com.mcm.core.Main.getTradution("s#UY?X&Zg66CvBm", uuid).substring(2) + ": "};
        if (event.getClickedInventory() != null && Arrays.asList(invsName).contains(event.getView().getTitle())) {
            event.setCancelled(true);



            if (event.getSlot() <= 35) {
                net.minecraft.server.v1_15_R1.ItemStack nms = CraftItemStack.asNMSCopy(event.getClickedInventory().getItem(event.getSlot()));
                NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();
                String id = compound.getString("id");

                if (ItensMercado.get(id) != null) {
                    double price = ItensMercado.get(id).getPrice();
                    if (CoinsDb.getCoins(uuid) >= price) {
                        player.sendMessage(com.mcm.core.Main.getTradution("?*HGUZ22%Mph9%k", uuid) + MercadoBuilder.formatter.format(price));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.getInventory().addItem(ItensMercado.get(id).getItem());
                        player.updateInventory();

                        CoinsDb.updateCoins(uuid, CoinsDb.getCoins(uuid) - (int) price);
                        CoinsDb.updateCoins(ItensMercado.get(id).getUuid(), CoinsDb.getCoins(ItensMercado.get(id).getUuid()) + (int) price);

                        MercadoDb.removeItem(id, false);
                        for (ServersList server : ServersList.values()) {
                            if (!server.name().replaceAll("_", "-").equals(com.mcm.core.Main.server_name))
                                RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/item_sold_market=" + id);
                        }
                    } else {
                        player.sendMessage(com.mcm.core.Main.getTradution("b3qUyt2@%F4N!hq", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(com.mcm.core.Main.getTradution("$*hZ7%FUJQp7k4@", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                        MercadoBuilder.categ(uuid, event.getView().getTitle(), PlayerPage.get(uuid).getPage());
                    }, 3L);
                }
            }

            if (event.getSlot() == 45) {
                if (event.getClickedInventory().getItem(event.getSlot()) != null && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                    if (event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.ARROW)) {
                        Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                            PlayerPage.get(uuid).setPage(PlayerPage.get(uuid).getPage() - 1);
                            player.openInventory(MercadoBuilder.categ(uuid, event.getView().getTitle(), PlayerPage.get(uuid).getPage()));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }, 3L);
                    } else if (event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.BOOK)) {
                        Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                            PlayerPage.get(uuid).setPage(0);
                            player.openInventory(MercadoBuilder.main(uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }, 3L);
                    }
                }
            }

            if (event.getSlot() == 46) {
                if (event.getClickedInventory().getItem(event.getSlot()) != null && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                        PlayerPage.get(uuid).setPage(0);
                        player.openInventory(MercadoBuilder.main(uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }, 3L);
                }
            }

            if (event.getSlot() == 53) {
                if (event.getClickedInventory().getItem(53) != null && !event.getClickedInventory().getItem(53).getType().equals(Material.AIR)) {
                    Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                        PlayerPage.get(uuid).setPage(PlayerPage.get(uuid).getPage() + 1);
                        player.openInventory(MercadoBuilder.categ(uuid, event.getView().getTitle(), PlayerPage.get(uuid).getPage()));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }, 3L);
                }
            }

            if (event.getSlot() == 48) {
                MercadoBuilder.openChestPrivate(player, uuid);
            }

            if (event.getSlot() == 49) {
                MercadoBuilder.openChestPrivate(player, uuid);
            }

            if (event.getSlot() == 50) {
                player.getOpenInventory().close();
                player.sendMessage(com.mcm.core.Main.getTradution("f3J$5vYVu5A&h&W", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }

        //chest private
        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase(com.mcm.core.Main.getTradution("ejgtfM8uhaNvDV@", uuid))) {
            event.setCancelled(true);

            int[] slots = {10, 11, 12, 13, 14, 15, 16, 19, 20, 21};
            if (event.getClickedInventory().getItem(event.getSlot()) != null && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                for (int slot : slots) {
                    if (slot == event.getSlot()) {
                        ItemStack item = event.getClickedInventory().getItem(event.getSlot()).clone();
                        if (InventoryHasSpace.hasSpace(player.getInventory(), item, item.getAmount()) == true) {
                            player.sendMessage(com.mcm.core.Main.getTradution("n5xy#rxyNcaq6VX", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.getInventory().addItem(item);
                            player.updateInventory();

                            net.minecraft.server.v1_15_R1.ItemStack nms = CraftItemStack.asNMSCopy(item);
                            NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();
                            String id = compound.getString("id");

                            MercadoDb.removeItem(id, false);

                            Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                                MercadoBuilder.openChestPrivate(player, uuid);
                            }, 3L);
                        } else {
                            player.sendMessage(com.mcm.core.Main.getTradution("59pd*!MjNzgB9$m", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                }
            }

            if (event.getSlot() == 40) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(MercadoBuilder.main(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 3L);
            }
        }

        //mercado menu
        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase(com.mcm.core.Main.getTradution("th!8@QHHZ2ws*T$", uuid))) {
            event.setCancelled(true);

            int[] slots = {11, 12, 13, 14, 15};
            if (Arrays.asList(slots).contains(event.getSlot())) {
                MercadoBuilder.openInv(player, uuid, event.getSlot());
            }

            if (event.getSlot() == 30) {
                MercadoBuilder.openChestPrivate(player, uuid);
            }

            if (event.getSlot() == 31) {
                MercadoBuilder.openChestPrivate(player, uuid);
            }

            if (event.getSlot() == 32) {
                player.getOpenInventory().close();
                player.sendMessage(com.mcm.core.Main.getTradution("f3J$5vYVu5A&h&W", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }
    }
}
