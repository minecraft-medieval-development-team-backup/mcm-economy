package com.mcm.economy.listeners;

import com.mcm.core.Main;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import java.text.DecimalFormat;
import java.util.Arrays;

public class CreateShopEvent implements Listener {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    @EventHandler
    public void onCreate(SignChangeEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        //25 placas
        //[Admin]     //ITEMNAME
        //Quantia: X  //Quantia:
        //C X        //C X
        //V X        //V X

        //[Loja]     //ITEMNAME
        //Quantia: X  //Quantia:
        //C X        //C X
        //V X        //V X

        String[] perm = {"superior", "gestor"};
        if (event.getLine(0) != null && event.getLine(0).equals("[Admin]") && Arrays.asList(perm).contains(tag)) {
            if (event.getLine(1) != null && event.getLine(1).contains("Quantia: ")) {
                if (event.getLine(2) != null) {
                    if (event.getLine(2).contains("C ") || event.getLine(2).contains("V ")) {
                        if (event.getLine(3) != null) {
                            if (event.getLine(3).contains("V ") && event.getLine(2).contains("C ")) {
                                try {
                                    int quantity = Integer.valueOf(event.getLine(1).replaceAll("Quantia: ", ""));
                                    int purchase = Integer.valueOf(event.getLine(2).replaceAll("C ", ""));
                                    int sale = Integer.valueOf(event.getLine(3).replaceAll("V ", ""));

                                    com.mcm.core.database.StoreDb.createStoreAdmin(event.getBlock().getLocation().getWorld().getName() + ":" + event.getBlock().getLocation().getBlockX() + ":" + event.getBlock().getLocation().getBlockY() + ":" + event.getBlock().getLocation().getBlockZ(), quantity, purchase, sale);
                                    event.setLine(0, ChatColor.RED + "##INDEFINIDO##");
                                    event.setLine(1, "Quantia: " + quantity);
                                    event.setLine(2, "C " + purchase);
                                    event.setLine(3, "V " + sale);

                                    player.sendMessage(Main.getTradution("AQ?FvcYSHyabw7y", uuid));
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } catch (NumberFormatException e) {
                                    player.sendMessage(Main.getTradution("bNN2@F6kf$xjX3b", uuid));
                                    sound(player, event.getBlock());
                                }
                            } else {
                                player.sendMessage(Main.getTradution("6nv6SfQDF5*WKsR", uuid));
                                sound(player, event.getBlock());
                            }
                        } else {
                            try {
                                int quantity = Integer.valueOf(event.getLine(1).replaceAll("Quantia: ", ""));
                                int purchase = 0;
                                int sale = 0;

                                if (event.getLine(2).contains("C ")) {
                                    purchase = Integer.valueOf(event.getLine(2).replaceAll("C ", ""));
                                } else if (event.getLine(2).contains("V "))
                                    sale = Integer.valueOf(event.getLine(2).replaceAll("V ", ""));

                                com.mcm.core.database.StoreDb.createStoreAdmin(event.getBlock().getLocation().getWorld().getName() + ":" + event.getBlock().getLocation().getBlockX() + ":" + event.getBlock().getLocation().getBlockY() + ":" + event.getBlock().getLocation().getBlockZ(), quantity, purchase, sale);
                                event.setLine(0, ChatColor.RED + "##INDEFINIDO##");
                                event.setLine(1, "Quantia: " + quantity);
                                if (event.getLine(2).contains("C ")) {
                                    event.setLine(2, "C " + purchase);
                                } else if (event.getLine(2).contains("V ")) {
                                    event.setLine(2, "V " + sale);
                                }

                                player.sendMessage(Main.getTradution("AQ?FvcYSHyabw7y", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } catch (NumberFormatException e) {
                                player.sendMessage(Main.getTradution("bNN2@F6kf$xjX3b", uuid));
                                sound(player, event.getBlock());
                            }
                        }
                    } else {
                        player.sendMessage(Main.getTradution("6nv6SfQDF5*WKsR", uuid));
                        sound(player, event.getBlock());
                    }
                } else {
                    player.sendMessage(Main.getTradution("k@3Dm8*GdUBvspg", uuid));
                    sound(player, event.getBlock());
                }
            } else {
                player.sendMessage(Main.getTradution("7Df$?zHeK7M4ESH", uuid));
                sound(player, event.getBlock());
            }
        }
    }

    private static void sound(Player player, Block sign) {
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        sign.breakNaturally();
    }
}
