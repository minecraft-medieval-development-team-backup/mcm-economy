package com.mcm.economy.listeners;

import com.mcm.core.Main;
import com.mcm.core.PluginMessage;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Arrays;

public class PlayerBreakShopEvent implements Listener {

    @EventHandler
    public void onBreak(final PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            String location = event.getClickedBlock().getLocation().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ();

            String[] perm = {"superior", "gestor"};
            if (player.getItemInHand() != null && player.getItemInHand().getType().equals(Material.STICK) && com.mcm.core.database.StoreDb.getStoreAdmin(location) != null && event.getClickedBlock().getType().name().contains("SIGN") && Arrays.asList(perm).contains(tag)) {
                com.mcm.core.database.StoreDb.removeStoreAdmin(location);
                PluginMessage.onPluginMessageSend("shop_admin_remove", location);
                player.sendMessage(Main.getTradution("gSsQ5jaqs@vzHtD", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                event.getClickedBlock().setType(Material.AIR);
            }
        }
    }
}
