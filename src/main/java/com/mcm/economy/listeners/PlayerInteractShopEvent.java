package com.mcm.economy.listeners;

import com.mcm.core.PluginMessage;
import com.mcm.core.cache.FixProblem1;
import com.mcm.core.database.CoinsDb;
import com.mcm.core.database.StoreDb;
import com.mcm.core.Main;
import com.mcm.core.utils.InventoryHasSpace;
import com.mcm.economy.managers.ShopBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;

public class PlayerInteractShopEvent implements Listener {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    @EventHandler (priority = EventPriority.HIGH)
    public void onInteract(final PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        //  /Material-id:quantia:vVALUE:cVALUE:vVEZES:cVEZES/
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock().getType().name().contains("SIGN")) {
            String location = event.getClickedBlock().getLocation().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ();
            if (StoreDb.getStoreAdmin(location) != null) {
                event.setCancelled(true);
                if (!player.isSneaking()) {
                    double value = StoreDb.getPurchaseAdmin(location);
                    double oldcoins = CoinsDb.getCoins(uuid);

                    double difference = 0;
                    for (String category : StoreDb.categories()) {
                        for (String itens : StoreDb.getItensGUI(category)) {
                            if (itens.contains(StoreDb.getItemAdmin(location))) {
                                String[] spl = itens.split(":");
                                difference = value - ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[5]), true);
                                if (difference == value) difference = 0;
                            }
                        }
                    }

                    if (oldcoins >= (value - difference)) {
                        int space = InventoryHasSpace.hasSpace(player.getInventory(), new ItemStack(Material.getMaterial(StoreDb.getItemAdmin(location).split("-")[0])));
                        Bukkit.broadcastMessage(String.valueOf(space));
                        if (space >= StoreDb.getQuantityAdmin(location)) {
                            CoinsDb.updateCoins(uuid, (int) (oldcoins - (value - difference)));

                            String[] split = StoreDb.getItemAdmin(location).split("-");
                            ItemStack item = new ItemStack(Material.getMaterial(split[0]), StoreDb.getQuantityAdmin(location), Short.parseShort(split[1]));
                            updateItemDesvalorization(uuid, item, (Sign) event.getClickedBlock().getState(), true, value);
                            player.getInventory().addItem(item);
                            player.sendMessage(Main.getTradution("?*HGUZ22%Mph9%k", uuid) + formatter.format(value - difference));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("R9zFuPN27*4!Bp9", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("b3qUyt2@%F4N!hq", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    double value = StoreDb.getPurchaseAdmin(location);
                    double oldcoins = CoinsDb.getCoins(uuid);

                    double difference = 0;
                    for (String category : StoreDb.categories()) {
                        for (String itens : StoreDb.getItensGUI(category)) {
                            if (itens.contains(StoreDb.getItemAdmin(location))) {
                                String[] spl = itens.split(":");
                                difference = value - ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[5]), true);
                                if (difference == value) difference = 0;
                            }
                        }
                    }

                    int space = InventoryHasSpace.hasSpace(player.getInventory(), new ItemStack(Material.getMaterial(StoreDb.getItemAdmin(location).split("-")[0])));
                    double perItem = (value - difference) / StoreDb.getQuantityAdmin(location);
                    //value - difference = (valor ajustado)
                    if (oldcoins >= perItem * space) {
                        if (space >= StoreDb.getQuantityAdmin(location)) {
                            CoinsDb.updateCoins(uuid, (int) (oldcoins - (perItem * space)));

                            String[] split = StoreDb.getItemAdmin(location).split("-");
                            ItemStack item = new ItemStack(Material.getMaterial(split[0]), space, Short.parseShort(split[1]));
                            player.getInventory().addItem(item);
                            updateItemDesvalorization(uuid, item, (Sign) event.getClickedBlock().getState(), true, value);
                            player.sendMessage(Main.getTradution("?*HGUZ22%Mph9%k", uuid) + formatter.format(perItem * space));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("R9zFuPN27*4!Bp9", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("b3qUyt2@%F4N!hq", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }
        }

        if (event.getAction().equals(Action.LEFT_CLICK_BLOCK) && event.getClickedBlock().getType().name().contains("SIGN")) {
            String location = event.getClickedBlock().getLocation().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ();
            if (StoreDb.getStoreAdmin(location) != null) {
                event.setCancelled(true);
                if (!player.isSneaking()) {
                    String[] split = StoreDb.getItemAdmin(location).split("-");
                    ItemStack item = new ItemStack(Material.getMaterial(split[0]), StoreDb.getQuantityAdmin(location), Short.parseShort(split[1]));

                    int quantity = 0;
                    for (int i = 0; i <= player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType().equals(item.getType()) && item.getDurability() == player.getInventory().getItem(i).getDurability()) {
                            quantity += player.getInventory().getItem(i).getAmount();
                        }
                    }

                    double value = StoreDb.getSaleAdmin(location);
                    double oldcoins = CoinsDb.getCoins(uuid);

                    double difference = 0;
                    for (String category : StoreDb.categories()) {
                        for (String itens : StoreDb.getItensGUI(category)) {
                            if (itens.contains(StoreDb.getItemAdmin(location))) {
                                String[] spl = itens.split(":");
                                difference = value - ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[4]), true);
                                if (difference == value) difference = 0;
                            }
                        }
                    }

                    if (quantity >= item.getAmount()) {
                        CoinsDb.updateCoins(uuid, (int) (oldcoins + (value + difference)));

                        com.mcm.core.utils.RemoveItemOfInventory.removeItemsFromType(player.getInventory(), item, item.getAmount());
                        updateItemDesvalorization(uuid, item, (Sign) event.getClickedBlock().getState(), false, value);
                        player.sendMessage(Main.getTradution("YBsDa3hgu%Ynp3f", uuid) + formatter.format(value + difference));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(Main.getTradution("#t5gvpqtwj%ZMGC", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    String[] split = StoreDb.getItemAdmin(location).split("-");
                    ItemStack item = new ItemStack(Material.getMaterial(split[0]), StoreDb.getQuantityAdmin(location), Short.parseShort(split[1]));

                    int quantity = 0;
                    for (int i = 0; i <= player.getInventory().getSize(); i++) {
                        if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType().equals(item.getType()) && item.getDurability() == player.getInventory().getItem(i).getDurability()) {
                            quantity += player.getInventory().getItem(i).getAmount();
                        }
                    }

                    double value = StoreDb.getSaleAdmin(location);
                    double oldcoins = CoinsDb.getCoins(uuid);

                    double difference = 0;
                    for (String category : StoreDb.categories()) {
                        for (String itens : StoreDb.getItensGUI(category)) {
                            if (itens.contains(StoreDb.getItemAdmin(location))) {
                                String[] spl = itens.split(":");
                                difference = value - ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[4]), true);
                                if (difference == value) difference = 0;
                            }
                        }
                    }

                    if (quantity >= item.getAmount()) {
                        CoinsDb.updateCoins(uuid, (int) (oldcoins + ((value + difference) * (quantity / 64))));

                        com.mcm.core.utils.RemoveItemOfInventory.removeItemsFromType(player.getInventory(), item, item.getAmount() * quantity);
                        updateItemDesvalorization(uuid, item, (Sign) event.getClickedBlock().getState(), false, value);
                        player.sendMessage(Main.getTradution("maPQV8DU3u?E6vx", uuid) + formatter.format((value + difference) * (quantity / 64)));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(Main.getTradution("#t5gvpqtwj%ZMGC", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            }
        }
    }

    private static void updateItemDesvalorization(String uuid, ItemStack item, Sign sign, boolean purchase, double value) {
        double porcentP = 0;
        double porcentS = 0;
        for (String category : StoreDb.categories()) {
            for (int i = 0; i < StoreDb.getItensGUI(category).size(); i++) {
                String[] spl = StoreDb.getItensGUI(category).get(i).split(":");
                if (spl[0].equalsIgnoreCase(item.getType().name() + "-" + item.getDurability())) {
                    String update = null;
                    for (String a : spl) {
                        if (update == null) {
                            update = a;
                        } else {
                            String type = null;
                            if (purchase == true) type = "cV";
                            else type = "vV";
                            if (a.contains(type)) {
                                String[] spl2 = a.split(type);
                                int v = Integer.valueOf(spl2[1]);
                                if (FixProblem1.get(uuid + item.getType().name() + "-" + item.getDurability() + type) == null) {
                                    v++;
                                    PluginMessage.onPluginMessageSend("update_fix_problem_economy_1", uuid + item.getType().name() + "-" + item.getDurability() + type);
                                }
                                update += ":" + type + v;
                            } else update += ":" + a;
                        }
                    }

                    //  /Material-id:quantia:vVALUE:cVALUE:vVEZES:cVEZES/
                    if (!StoreDb.getItensGUI(category).get(i).equals(update)) {
                        StoreDb.updateItemGUI(category, StoreDb.getItensGUI(category).get(i), update);
                    }

                    if (purchase) {
                        if (sign.getLine(1).contains("C ")) {
                            if (ShopBuilder.getPorcent(spl[5]) != 0) {
                                sign.setLine(1, "C " + formatter.format(ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[5]), true)));
                            } else {
                                sign.setLine(1, "C " + formatter.format(value));
                            }
                        } else {
                            if (ShopBuilder.getPorcent(spl[5]) != 0) {
                                sign.setLine(2, "C " + formatter.format(ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[5]), true)));
                            } else {
                                sign.setLine(2, "C" + formatter.format(value));
                            }
                        }
                    } else {
                        if (ShopBuilder.getPorcent(spl[4]) != 0) {
                            sign.setLine(2, "V " + formatter.format(ShopBuilder.difference(value, ShopBuilder.getPorcent(spl[4]), false)));
                        } else {
                            sign.setLine(2, "V " + formatter.format(value));
                        }
                    }

                    if (ShopBuilder.getPorcent(spl[5]) != 0 && ShopBuilder.getPorcent(spl[4]) != 0) {
                        sign.setLine(3, ChatColor.BOLD + "C" + (int) ShopBuilder.getPorcent(spl[5]) + "% V" + (int) ShopBuilder.getPorcent(spl[4]) + "%");
                    } else if (ShopBuilder.getPorcent(spl[5]) == 0 && ShopBuilder.getPorcent(spl[4]) != 0) {
                        sign.setLine(3, ChatColor.BOLD + "C0% V" + (int) ShopBuilder.getPorcent(spl[4]) + "%");
                    } else if (ShopBuilder.getPorcent(spl[5]) != 0 && ShopBuilder.getPorcent(spl[4]) == 0) {
                        sign.setLine(3, ChatColor.BOLD + "C" + (int) ShopBuilder.getPorcent(spl[5]) + "% V0%");
                    }
                    sign.update();
                }
            }
        }
    }
}
