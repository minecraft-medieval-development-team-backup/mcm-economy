package com.mcm.economy.listeners;

import com.mcm.core.Main;
import com.mcm.core.PluginMessage;
import com.mcm.economy.managers.ShopBuilder;
import org.bukkit.*;
import org.bukkit.block.Sign;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.EulerAngle;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;

public class SetItemOnShopEvent implements Listener {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    @EventHandler (priority = EventPriority.MONITOR)
    public void onSetItem(final PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock().getType().name().contains("SIGN") && player.getItemInHand() != null && player.isSneaking()) {
            String location = event.getClickedBlock().getLocation().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ();

            if (com.mcm.core.database.StoreDb.getItemAdmin(location) == null && Arrays.asList(perm).contains(tag)) {
                Sign sign = (Sign) event.getClickedBlock().getState();

                double purchase = com.mcm.core.database.StoreDb.getPurchaseAdmin(location);
                double sale = com.mcm.core.database.StoreDb.getSaleAdmin(location);

                double porcentP = 0;
                double porcentS = 0;

                //  /Material-id:quantia:vVALUE:cVALUE:vVEZES:cVEZES/
                for (String category : com.mcm.core.database.StoreDb.categories()) {
                    for (String data : com.mcm.core.database.StoreDb.getItensGUI(category)) {
                        String[] split = data.split(":");

                        if (split[0].equals(player.getItemInHand().getType().name() + "-" + player.getItemInHand().getDurability())) {
                            double valueP = Double.parseDouble(split[3].replaceAll("c", ""));
                            porcentP = ShopBuilder.difference(valueP, ShopBuilder.getPorcent(split[5]), true);
                            double valueS = Double.parseDouble(split[2].replaceAll("v", ""));
                            porcentS = ShopBuilder.difference(valueS, ShopBuilder.getPorcent(split[4]), true);
                        }
                    }
                }

                String signInfo = null;
                //org.bukkit.material.Sign signData = (org.bukkit.material.Sign) sign.getBlock().getState().getData();
                if (purchase != 0 && sale != 0) {
                    sign.setLine(0, sign.getLine(1));
                    sign.setLine(1, "C " + formatter.format(purchase));
                    sign.setLine(2, "V " + formatter.format(sale));
                    sign.setLine(3, ChatColor.GREEN + "C" + (int) porcentP + "% " + ChatColor.RED + "V" + (int) porcentS + "%");
                    sign.update();
                    //signInfo = signData.getFacing().name() + ":" + sign.getLine(0) + ":" + sign.getLine(1) + ":" + sign.getLine(2) + ":" + sign.getLine(3);
                } else {
                    sign.setLine(0, ChatColor.BOLD + player.getItemInHand().getType().name());
                    if (purchase != 0) {
                        sign.setLine(2, "C " + formatter.format(purchase));
                    } else sign.setLine(2, "V " + formatter.format(sale));
                    sign.setLine(3, ChatColor.GREEN + "C" + (int) porcentP + "% " + ChatColor.RED + "V" + (int) porcentS + "%");
                    sign.update();
                    //signInfo = signData.getFacing().name() + ":" + sign.getLine(0) + ":" + sign.getLine(1) + ":" + sign.getLine(2) + ":" + sign.getLine(3);
                }

                String asInfo = null;
                if (!player.getItemInHand().getType().isBlock()) {
                    Location loc = new Location(sign.getWorld(), sign.getX() + 1.25, sign.getY() - 2.20, sign.getZ() + 0.45, (float) 90, (float) 0);
                    ArmorStand armorStand = sign.getWorld().spawn(loc, ArmorStand.class);
                    int id = new Random().nextInt(999999999);
                    armorStand.setCustomName(String.valueOf(id));
                    armorStand.setCustomNameVisible(false);
                    EulerAngle headPose = new EulerAngle(1.55, 0, 0);
                    armorStand.setHeadPose(headPose);
                    armorStand.setSmall(false);
                    armorStand.setVisible(false);
                    armorStand.setGravity(false);
                    armorStand.setHelmet(player.getItemInHand());
                    asInfo = loc.getWorld().getName() + ":" + loc.getX() + ":" + loc.getY() + ":" + loc.getZ() + ":" + loc.getYaw() + ":" + loc.getPitch() + ":" + id + ":false:" + headPose.getX() + ":" + headPose.getY() + ":" + headPose.getZ() + ":false:false:false:" + player.getItemInHand().getType().name() + ":" + player.getItemInHand().getDurability();
                } else {
                    Location loc = new Location(sign.getWorld(), sign.getX() + 0.5, sign.getY() - 1.2, sign.getZ() + 0.5, (float) 90, (float) 0);
                    ArmorStand armorStand = sign.getWorld().spawn(loc, ArmorStand.class);
                    int id = new Random().nextInt(999999999);
                    armorStand.setCustomName(String.valueOf(id));
                    armorStand.setCustomNameVisible(false);
                    EulerAngle headPose = new EulerAngle(0, 4.8, 0);
                    armorStand.setHeadPose(headPose);
                    armorStand.setSmall(true);
                    armorStand.setVisible(false);
                    armorStand.setGravity(false);
                    armorStand.setHelmet(player.getItemInHand());
                    asInfo = loc.getWorld().getName() + ":" + loc.getX() + ":" + loc.getY() + ":" + loc.getZ() + ":" + loc.getYaw() + ":" + loc.getPitch() + ":" + id + ":false:" + headPose.getX() + ":" + headPose.getY() + ":" + headPose.getZ() + ":true:false:false:" + player.getItemInHand().getType().name() + ":" + player.getItemInHand().getDurability();
                }

                // world:x:y:z/material-durability:quantity:purchase:sale/sign.getLine(0):sign.getLine(1):sign.getLine(2):sign.getLine(3)/world:x:y:z:id:customnamevisible:headpose-x:headpose-y:headpose-z:small:visible:gravity:material:durability
                //String update = location + "/" + player.getItemInHand().getType().name() + "-" + player.getItemInHand().getDurability() + ":" + com.mcm.core.database.StoreDb.getQuantityAdmin(location) + ":" + purchase + ":" + sale + "/" + signInfo + "/" + asInfo;
                //PluginMessage.onPluginMessageSend("shop_admin_create", update);

                com.mcm.core.database.StoreDb.updateItem(location, player.getItemInHand().getType().name() + "-" + player.getItemInHand().getDurability());
                player.sendMessage(Main.getTradution("6DYNRm3zhv6%##f", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }
}
