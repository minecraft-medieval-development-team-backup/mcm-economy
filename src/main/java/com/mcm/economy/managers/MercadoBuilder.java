package com.mcm.economy.managers;

import com.mcm.core.Main;
import com.mcm.core.cache.ItensMercado;
import com.mcm.core.database.MercadoDb;
import com.mcm.core.utils.EnchantmentGlow;
import com.mcm.core.utils.RemoveAllFlags;
import com.mcm.core.utils.TimeUtil;
import com.mcm.economy.cache.PlayerPage;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import net.minecraft.server.v1_15_R1.NBTTagString;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

public class MercadoBuilder {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static Inventory categ(String uuid, String category, int page) {
        String invName = null;
        if (category.equalsIgnoreCase("pvp")) invName = Main.getTradution("AmgCX!5M9gV@*!J", uuid);
        else if (category.equalsIgnoreCase("armaduras")) invName = Main.getTradution("W#3mA8F$tjXkN*j", uuid);
        else if (category.equalsIgnoreCase("mobspawners")) invName = Main.getTradution("v4G?ekFct23Dvvc", uuid);
        else if (category.equalsIgnoreCase("poções")) invName = Main.getTradution("P8DRv#g*p?E4*nE", uuid);
        else if (category.equalsIgnoreCase("minérios")) invName = Main.getTradution("s#UY?X&Zg66CvBm", uuid);
        Inventory inventory = Bukkit.createInventory(null, 6 * 9, invName.substring(2) + ": ");

        int limit = 35;

        if (MercadoDb.getItens(category).isEmpty()) return null;

        int i = 1;
        int slot = 0;
        boolean haveMore = false;
        for (ItensMercado itemData : MercadoDb.getItens(category)) {
            if ((itemData.getPlaced().getTime() - new Date().getTime()) / 1000 <= 0) continue;
            if (page > 1) {
                if (slot > limit) {
                    haveMore = true;
                    break;
                }
                if ((page * limit) > i) {
                    inventory.setItem(slot, item(itemData, uuid));
                    slot++;
                } else i++;
            } else {
                if (slot > limit) {
                    haveMore = true;
                    break;
                }
                inventory.setItem(slot, item(itemData, uuid));
                slot++;
            }
        }

        ItemStack itens = new ItemStack(Material.BARREL);
        ItemMeta metaItens = itens.getItemMeta();
        metaItens.setDisplayName(Main.getTradution("e6RAe?rq%nyVvFN", uuid));
        ArrayList<String> loreItens = new ArrayList<>();
        loreItens.add(" ");
        loreItens.add(Main.getTradution("5CSn&!4brttBGvt", uuid));
        metaItens.setLore(loreItens);
        itens.setItemMeta(metaItens);

        ItemStack remove = new ItemStack(Material.BARRIER);
        ItemMeta metaRmv = remove.getItemMeta();
        metaRmv.setDisplayName(Main.getTradution("ytC!RXWVRup3UFU", uuid));
        ArrayList<String> loreRmv = new ArrayList<>();
        loreRmv.add(" ");
        loreRmv.add(Main.getTradution("HP3#Dt&!NhHXDz7", uuid));
        metaRmv.setLore(loreRmv);
        remove.setItemMeta(metaRmv);

        ItemStack add = new ItemStack(Material.MAP);
        ItemMeta metaAdd = add.getItemMeta();
        metaAdd.setDisplayName(Main.getTradution("RSS2@$b9?NSQbQD", uuid));
        ArrayList<String> loreAdd = new ArrayList<>();
        loreAdd.add(" ");
        loreAdd.add(Main.getTradution("pjrDfSrP@Y2cZ&E", uuid));
        metaAdd.setLore(loreAdd);
        add.setItemMeta(metaAdd);

        ItemStack back = new ItemStack(Material.ARROW);
        ItemMeta metaBack = back.getItemMeta();
        metaBack.setDisplayName(Main.getTradution("NF@p4kK&Pk5Z$V%", uuid));
        back.setItemMeta(metaBack);

        ItemStack next = new ItemStack(Material.ARROW);
        ItemMeta metaNext = next.getItemMeta();
        metaNext.setDisplayName(Main.getTradution("3FjrEcaT&dJq8!G", uuid));
        next.setItemMeta(metaNext);

        ItemStack menu = new ItemStack(Material.BOOK);
        ItemMeta meta = menu.getItemMeta();
        meta.setDisplayName(com.mcm.core.Main.getTradution("R6G7@ZqW4BVzUbT", uuid));
        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(com.mcm.core.Main.getTradution("Zzh5y3?7EFzR3Ff", uuid));
        meta.setLore(lore);
        menu.setItemMeta(meta);
        inventory.setItem(40, menu);

        inventory.setItem(48, remove);
        inventory.setItem(49, itens);
        inventory.setItem(50, add);
        if (page >= 2) inventory.setItem(45, back);
        if (slot == limit && haveMore == true) inventory.setItem(53, next);
        if (page >= 2) inventory.setItem(46, menu); else inventory.setItem(45, menu);

        return inventory;
    }

    private static ItemStack item(ItensMercado itemData, String uuid) {
        ItemStack item = itemData.getItem().clone();
        ItemMeta meta = item.getItemMeta();
        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(Main.getTradution("$xu8Mz8$R@JKxkD", uuid) + ChatColor.GRAY + formatter.format(itemData.getPrice()));
        lore.add(Main.getTradution("b#paxC*96KEByaV", uuid) + ChatColor.GRAY + itemData.getName());
        lore.add(Main.getTradution("8?&9NaRvZzbhU!k", uuid) + ChatColor.GRAY + TimeUtil.getTime((itemData.getPlaced().getTime() - new Date().getTime()) / 1000));
        lore.add(" ");
        lore.add(Main.getTradution("D#s7?yFM#ej37J8", uuid));
        meta.setLore(lore);
        meta.setLocalizedName(itemData.getUuid());
        item.setItemMeta(meta);
        return item;
    }

    public static Inventory main(String uuid) {
        ItemStack pocoes = new ItemStack(Material.POTION);
        ItemMeta metaPocoes = pocoes.getItemMeta();
        metaPocoes.setDisplayName(Main.getTradution("P8DRv#g*p?E4*nE", uuid));
        ArrayList<String> lorePoce = new ArrayList<>();
        lorePoce.add(" ");
        lorePoce.add(Main.getTradution("#QjW8E58DfeYJ%a", uuid));
        metaPocoes.setLore(lorePoce);
        pocoes.setItemMeta(metaPocoes);
        pocoes = EnchantmentGlow.addGlow(pocoes);

        ItemStack minerios = new ItemStack(Material.IRON_ORE);
        ItemMeta metaMinerios = minerios.getItemMeta();
        metaMinerios.setDisplayName(Main.getTradution("s#UY?X&Zg66CvBm", uuid));
        ArrayList<String> loreMine = new ArrayList<>();
        loreMine.add(" ");
        loreMine.add(Main.getTradution("#QjW8E58DfeYJ%a", uuid));
        metaMinerios.setLore(loreMine);
        minerios.setItemMeta(metaMinerios);

        ItemStack mobspawner = new ItemStack(Material.SPAWNER);
        ItemMeta metaMobspawner = mobspawner.getItemMeta();
        metaMobspawner.setDisplayName(Main.getTradution("v4G?ekFct23Dvvc", uuid));
        ArrayList<String> loreMob = new ArrayList<>();
        loreMob.add(" ");
        loreMob.add(Main.getTradution("#QjW8E58DfeYJ%a", uuid));
        metaMobspawner.setLore(loreMob);
        mobspawner.setItemMeta(metaMobspawner);

        ItemStack armadura = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
        ItemMeta metaArmadura = armadura.getItemMeta();
        metaArmadura.setDisplayName(Main.getTradution("W#3mA8F$tjXkN*j", uuid));
        ArrayList<String> loreArm = new ArrayList<>();
        loreArm.add(" ");
        loreArm.add(Main.getTradution("#QjW8E58DfeYJ%a", uuid));
        metaArmadura.setLore(loreArm);
        armadura.setItemMeta(metaArmadura);

        ItemStack pvp = new ItemStack(Material.GOLDEN_SWORD);
        ItemMeta metaPVP = pvp.getItemMeta();
        metaPVP.setDisplayName(Main.getTradution("AmgCX!5M9gV@*!J", uuid));
        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(Main.getTradution("#QjW8E58DfeYJ%a", uuid));
        metaPVP.setLore(lore);
        pvp.setItemMeta(metaPVP);

        ItemStack itens = new ItemStack(Material.BARREL);
        ItemMeta metaItens = itens.getItemMeta();
        metaItens.setDisplayName(Main.getTradution("e6RAe?rq%nyVvFN", uuid));
        ArrayList<String> loreItens = new ArrayList<>();
        loreItens.add(" ");
        loreItens.add(Main.getTradution("5CSn&!4brttBGvt", uuid));
        metaItens.setLore(loreItens);
        itens.setItemMeta(metaItens);

        ItemStack remove = new ItemStack(Material.BARRIER);
        ItemMeta metaRmv = remove.getItemMeta();
        metaRmv.setDisplayName(Main.getTradution("ytC!RXWVRup3UFU", uuid));
        ArrayList<String> loreRmv = new ArrayList<>();
        loreRmv.add(" ");
        loreRmv.add(Main.getTradution("HP3#Dt&!NhHXDz7", uuid));
        metaRmv.setLore(loreRmv);
        remove.setItemMeta(metaRmv);

        ItemStack add = new ItemStack(Material.MAP);
        ItemMeta metaAdd = add.getItemMeta();
        metaAdd.setDisplayName(Main.getTradution("RSS2@$b9?NSQbQD", uuid));
        ArrayList<String> loreAdd = new ArrayList<>();
        loreAdd.add(" ");
        loreAdd.add(Main.getTradution("pjrDfSrP@Y2cZ&E", uuid));
        metaAdd.setLore(loreAdd);
        add.setItemMeta(metaAdd);

        Inventory inventory = Bukkit.createInventory(null, 4 * 9, Main.getTradution("th!8@QHHZ2ws*T$", uuid));
        inventory.setItem(11, RemoveAllFlags.remove(pvp));
        inventory.setItem(12, RemoveAllFlags.remove(armadura));
        inventory.setItem(13, RemoveAllFlags.remove(pocoes));
        inventory.setItem(14, mobspawner);
        inventory.setItem(15, minerios);
        inventory.setItem(30, remove);
        inventory.setItem(31, itens);
        inventory.setItem(32, add);

        return inventory;
    }

    public static void openInv(Player player, String uuid, int slot) {
        String categ = null;
        if (slot == 11) categ = "pvp"; else if (slot == 12) categ = "armaduras";  else if (slot == 13) categ = "poções"; else if (slot == 14) categ = "mobspawner"; else if (slot == 15) categ = "minérios";
        if (MercadoBuilder.categ(uuid, categ, 1) != null) {
            String finalCateg = categ;
            Bukkit.getScheduler().runTaskLater(com.mcm.economy.Main.plugin, () -> {
                player.openInventory(MercadoBuilder.categ(uuid, finalCateg, 1));
                if (PlayerPage.get(uuid) == null) new PlayerPage(uuid, finalCateg, 1); else PlayerPage.get(uuid).setCategory(finalCateg); PlayerPage.get(uuid).setPage(1);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }, 3L);
        } else {
            player.sendMessage(com.mcm.core.Main.getTradution("qCx!R5kR##TrXSQ", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }

    public static void openChestPrivate(Player player, String uuid) {
        if (ItensMercado.uuidList != null && ItensMercado.uuidList.contains(uuid)) {
            Inventory inventory = Bukkit.createInventory(null, 5 * 9, com.mcm.core.Main.getTradution("ejgtfM8uhaNvDV@", uuid));
            int slot = 10;
            for (String id : ItensMercado.idList) {
                if (ItensMercado.get(id).getUuid().equalsIgnoreCase(uuid)) {
                    ItemStack item = ItensMercado.get(id).getItem().clone();

                    net.minecraft.server.v1_15_R1.ItemStack nms = CraftItemStack.asNMSCopy(item);
                    NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();
                    compound.set("id", NBTTagString.a(id));
                    nms.setTag(compound);
                    item = CraftItemStack.asBukkitCopy(nms);

                    inventory.setItem(slot, item);
                    slot++;
                    if (slot == 17) slot = 19;
                    if (slot == 21) break;
                }
            }

            ItemStack menu = new ItemStack(Material.BOOK);
            ItemMeta meta = menu.getItemMeta();
            meta.setDisplayName(com.mcm.core.Main.getTradution("R6G7@ZqW4BVzUbT", uuid));
            ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(com.mcm.core.Main.getTradution("Zzh5y3?7EFzR3Ff", uuid));
            meta.setLore(lore);
            menu.setItemMeta(meta);
            inventory.setItem(40, menu);

            Bukkit.getScheduler().runTaskLater(com.mcm.economy.Main.plugin, () -> {
                player.openInventory(inventory);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }, 3L);
        } else {
            player.sendMessage(com.mcm.core.Main.getTradution("gnY*s?7zmFebKQm", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }
}
