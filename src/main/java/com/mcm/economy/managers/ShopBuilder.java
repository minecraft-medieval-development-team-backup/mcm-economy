package com.mcm.economy.managers;

import com.mcm.core.Main;
import com.mcm.core.cache.Last24h;
import com.mcm.core.database.TagDb;
import com.mcm.economy.cache.PlayerPage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShopBuilder {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static double porcentVIP = 15.0;
    private static double porcentLimit = 45.0;

    public static double getPorcent(String info) {
        Bukkit.broadcastMessage("Last24h: " + Last24h.last24h);
        info = info.split("#")[0];
        if (info.contains("vV")) {
            double value = (double) Integer.valueOf(info.replaceAll("vV", "")) / (double) Last24h.last24h * 100.0;
            double result = porcentLimit / 100.0 * value;
            if (result > porcentLimit) result = 45;
            if (String.valueOf(result).equals("NaN")) return 0; else return result;
        } else {
            double value = (double) Integer.valueOf(info.replaceAll("cV", "")) / (double) Last24h.last24h * 100.0;
            double result = porcentLimit / 100.0 * value;
            if (result > porcentLimit) result = 45;
            if (String.valueOf(result).equals("NaN")) return 0; else return result;
        }
    }

    public static double difference(double value, double porcent, boolean positive) {
        if (porcent <= 0) return 0;

        double result = 0;
        if (positive) {
            result = value / 100.0 * (100.0 + porcent);
        } else result = value / 100.0 * (100.0 - porcent);

        return result;
    }

    private static ChatColor getColor(double porcent) {
        if (porcent <= 15) return ChatColor.GREEN;
        else if (porcent <= 30) return ChatColor.GOLD;
        else if (porcent <= 45) return ChatColor.RED;
        else return ChatColor.GREEN;
    }

    public static Inventory itens(String uuid, String category, int page) {
        int itensAdded = 0;

        if (PlayerPage.get(uuid) == null) {
            new PlayerPage(uuid, category, page).insert();
        } else PlayerPage.get(uuid).setPage(page);

        List<String> itens = new ArrayList<String>();

        if (page > 1) {
            for (int i = 28 * (page - 1); i <= com.mcm.core.database.StoreDb.getItensGUI(category).size(); i++) {
                itens.add(com.mcm.core.database.StoreDb.getItensGUI(category).get(i - 1));
            }
        } else if (page == 1) itens = com.mcm.core.database.StoreDb.getItensGUI(category);

        int size = 3;
        if (itens.size() <= 7) size = 3;
        else if (itens.size() <= 14) size = 4;
        else if (itens.size() <= 21) size = 5;
        else if (itens.size() <= 28 || itens.size() > 28) size = 6;
        Inventory inventory = Bukkit.createInventory(null, size * 9, com.mcm.core.Main.getTradution("8G6YG#g6VM!VYgm", uuid));

        int line = 0;
        for (String data : itens) {
            if (itensAdded <= 6) line = 10;
            else if (itensAdded <= 13) line = 12;
            else if (itensAdded <= 20) line = 14;
            else if (itensAdded <= 27) line = 16;

            String[] split = data.split(":");

            //  /Material-id:quantia:vVALUE:cVALUE:vVEZES:cVEZES/
            if (itensAdded <= 27) {
                String[] material = split[0].split("-");
                ItemStack item = new ItemStack(Material.getMaterial(material[0]), Integer.valueOf(split[1]), Short.parseShort(material[1]));
                ItemMeta meta = item.getItemMeta();
                ArrayList<String> lore = new ArrayList<String>();
                lore.add(Main.getTradution("H63kx78S#sF9fcq", uuid));
                lore.add(" ");
                lore.add(Main.getTradution("b3N3RyVtv@6#rBJ", uuid) + ChatColor.GRAY + split[1]);
                if (!split[3].equals("c0")) {
                    double value = Double.valueOf(split[3].replaceAll("c", "")) - (porcentVIP * (Double.valueOf(split[2].replaceAll("v", "")) / 100));
                    if (getPorcent(split[5]) == 0) {
                        lore.add(Main.getTradution("@ET2qQ2a?$TF3#w", uuid) + getColor(getPorcent(split[5])) + "$" + formatter.format(value) + Main.getTradution("D9cuC$svRgQhttJ", uuid) + getColor(getPorcent(split[5])) + String.valueOf(getPorcent(split[5])) + "%" + ChatColor.YELLOW + ")");
                    } else {
                        lore.add(Main.getTradution("@ET2qQ2a?$TF3#w", uuid) + ChatColor.DARK_GRAY + ChatColor.STRIKETHROUGH + "(" + value + ")" + getColor(getPorcent(split[5])) + " $" + formatter.format(difference(value, getPorcent(split[5]), true)) + Main.getTradution("D9cuC$svRgQhttJ", uuid) + getColor(getPorcent(split[5])) + String.valueOf(getPorcent(split[5])) + "%" + ChatColor.YELLOW + ")");
                    }
                }
                if (!split[2].equals("v0")) {
                    double value = Double.valueOf(split[2].replaceAll("v", "")) + (porcentVIP * (Double.valueOf(split[2].replaceAll("v", "")) / 100));
                    if (getPorcent(split[4]) == 0) {
                        lore.add(Main.getTradution("e38P9@w?e?VTe9j", uuid) + getColor(getPorcent(split[4])) + "$" + formatter.format(value) + Main.getTradution("D9cuC$svRgQhttJ", uuid) + getColor(getPorcent(split[4])) + String.valueOf(getPorcent(split[4])) + "%" + ChatColor.YELLOW + ")");
                    } else {
                        lore.add(Main.getTradution("e38P9@w?e?VTe9j", uuid) + ChatColor.DARK_GRAY + ChatColor.STRIKETHROUGH + "(" + value + ")" + getColor(getPorcent(split[4])) + " $" + formatter.format(difference(value, getPorcent(split[4]), false)) + Main.getTradution("D9cuC$svRgQhttJ", uuid) + getColor(getPorcent(split[4])) + String.valueOf(getPorcent(split[4])) + "%" + ChatColor.YELLOW + ")");
                    }
                }
                lore.add(" ");
                lore.add(Main.getTradution("%QjNCsFh*7#3BXJ", uuid));
                meta.setLore(lore);
                meta.setLocalizedName(data);
                String[] permView = {"superior", "gestor"};
                if (Arrays.asList(permView).contains(TagDb.getTag(uuid))) meta.setDisplayName(item.getType().name() + "  " + ChatColor.RED + "(ID: " + split[split.length - 1] + ")"); else meta.setDisplayName(item.getType().name());
                item.setItemMeta(meta);
                inventory.setItem(itensAdded + line, item);
                itensAdded++;
            } else break;
        }

        ItemStack back = new ItemStack(Material.ARROW);
        ItemMeta metaBack = back.getItemMeta();
        metaBack.setDisplayName(Main.getTradution("NF@p4kK&Pk5Z$V%", uuid));
        back.setItemMeta(metaBack);

        ItemStack next = new ItemStack(Material.ARROW);
        ItemMeta metaNext = next.getItemMeta();
        metaNext.setDisplayName(Main.getTradution("3FjrEcaT&dJq8!G", uuid));
        next.setItemMeta(metaNext);

        if (page == 1 && itens.size() > 27) {
            inventory.setItem(53, next);
        } else if (page > 1) {
            if (inventory.getSize() <= 27) {
                inventory.setItem(18, back);
            } else if (inventory.getSize() <= 35) {
                inventory.setItem(27, back);
            } else if (inventory.getSize() <= 44) {
                inventory.setItem(36, back);
            } else if (inventory.getSize() <= 53) {
                inventory.setItem(45, back);
                if (itens.size() > 27) {
                    inventory.setItem(53, next);
                }
            }
        }

        return inventory;
    }

    public static Inventory categories(String uuid) {
        int itensAdded = 0;

        List<String> categories = com.mcm.core.database.StoreDb.categories();

        if (categories == null) return null;

        int size = 3;
        if (categories.size() <= 7) size = 3;
        else if (categories.size() <= 14) size = 4;
        else if (categories.size() <= 21) size = 5;
        else if (categories.size() <= 28) size = 6;
        Inventory inventory = Bukkit.createInventory(null, size * 9, Main.getTradution("K7q9?n5tM$7s@8U", uuid));

        int line = 0;
        for (String category : categories) {
            if (itensAdded <= 6) line = 10;
            else if (itensAdded <= 13) line = 12;
            else if (itensAdded <= 20) line = 14;
            else if (itensAdded <= 27) line = 16;


            if (itensAdded <= 27) {
                ItemStack categ = com.mcm.core.database.StoreDb.getCategoryItemGUI(category);
                ItemMeta meta = categ.getItemMeta();
                ArrayList<String> lore = new ArrayList<>();
                lore.add(Main.getTradution("P!X3rRN2PB5dSdC", uuid));
                lore.add(ChatColor.GRAY + String.valueOf((int) porcentVIP) + Main.getTradution("PzX$kmjuTX5kg?U", uuid));
                lore.add(" ");
                lore.add(Main.getTradution("Yd2%W&fshN*pCUq", uuid));
                inventory.setItem(itensAdded + line, categ);
                itensAdded++;
            }
        }

        return inventory;
    }

}
