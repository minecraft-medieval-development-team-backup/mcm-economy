package com.mcm.economy.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class CategorySeparator {

    public static String getCategory(ItemStack item) {
        Material[] pvp = {Material.STONE_SWORD, Material.DIAMOND_SWORD, Material.GOLDEN_SWORD, Material.IRON_SWORD, Material.WOODEN_SWORD, Material.BOW, Material.DIAMOND_AXE, Material.GOLDEN_AXE, Material.IRON_AXE, Material.STONE_AXE, Material.WOODEN_AXE, Material.SHIELD
                , Material.TRIDENT, Material.CROSSBOW, Material.TOTEM_OF_UNDYING};
        Material[] armaduras = {Material.CHAINMAIL_CHESTPLATE, Material.DIAMOND_CHESTPLATE, Material.GOLDEN_CHESTPLATE, Material.IRON_CHESTPLATE, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.CHAINMAIL_LEGGINGS, Material.DIAMOND_LEGGINGS, Material.GOLDEN_LEGGINGS
                , Material.IRON_LEGGINGS, Material.CHAINMAIL_HELMET, Material.DIAMOND_HELMET, Material.GOLDEN_HELMET, Material.IRON_HELMET, Material.IRON_HELMET, Material.LEATHER_HELMET, Material.TURTLE_HELMET, Material.CHAINMAIL_BOOTS, Material.DIAMOND_BOOTS, Material.GOLDEN_BOOTS
                , Material.IRON_BOOTS, Material.LEATHER_BOOTS};
        Material[] mobspawner = {Material.SPAWNER};
        Material[] minerios = {Material.COAL_ORE, Material.DIAMOND_ORE, Material.EMERALD_ORE, Material.GOLD_ORE, Material.IRON_ORE, Material.LAPIS_ORE, Material.NETHER_QUARTZ_ORE, Material.REDSTONE_ORE, Material.COAL_BLOCK, Material.DIAMOND_BLOCK, Material.EMERALD_BLOCK, Material.GOLD_BLOCK
                , Material.IRON_BLOCK, Material.LAPIS_BLOCK, Material.REDSTONE_BLOCK, Material.COAL, Material.DIAMOND, Material.EMERALD, Material.GOLD_INGOT, Material.IRON_INGOT, Material.LAPIS_LAZULI, Material.QUARTZ, Material.REDSTONE};
        Material[] pocoes = {Material.POTION, Material.LINGERING_POTION, Material.SPLASH_POTION};

        if (Arrays.asList(pvp).contains(item.getType())) {
            return "pvp";
        } else if (Arrays.asList(armaduras).contains(item.getType())) {
            return "armaduras";
        } else if (Arrays.asList(mobspawner).contains(item.getType())) {
            return "mobspawners";
        } else if (Arrays.asList(minerios).contains(item.getType())) {
            return "minérios";
        } else if (Arrays.asList(pocoes).contains(item.getType())) {
            return "poções";
        } else {
            return null;
        }
    }
}
